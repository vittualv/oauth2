package it.vittualv.oauth2.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;

import it.vittualv.oauth2.service.mvc.Oausth2ServiceWebMvcConfigurerAdapter;

/**
 * 
 * @author VittuAlv
 *
 */
@EnableAutoConfiguration
@Import(value={Oausth2ServiceWebMvcConfigurerAdapter.class})
public class OauthServiceApplication {

	
	 public static void main(String[] args) throws Exception {
	        SpringApplication.run(OauthServiceApplication.class, args);
	    }
}
