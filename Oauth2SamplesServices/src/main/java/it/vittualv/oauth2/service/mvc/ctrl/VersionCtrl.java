package it.vittualv.oauth2.service.mvc.ctrl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 * @author VittuAlv
 *
 */
@RestController
public class VersionCtrl {
	
	@RequestMapping("/")
	public String version(){
		return "1.0.0.M1";
	}

}
