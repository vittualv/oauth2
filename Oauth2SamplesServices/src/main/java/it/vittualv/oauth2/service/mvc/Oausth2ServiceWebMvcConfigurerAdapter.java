package it.vittualv.oauth2.service.mvc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import it.vittualv.oauth2.service.mvc.conf.OAuth2ServerConfiguration;
import it.vittualv.oauth2.service.mvc.security.conf.Oauth2WebSecurityConfiguration;

/**
 * 
 * @author VittuAlv
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(value={"it.vittualv.oauth2.service.mvc.ctrl"})
@Import(value={Oauth2WebSecurityConfiguration.class, OAuth2ServerConfiguration.class})
public class Oausth2ServiceWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

}