package it.vittualv.oauth2.service.mvc.security;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


/**
 * 
 * @author VittuAlv
 *
 */
@Component
public class CustomUserDetailsService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		return new CustomUserDetails(username);
	}
	
	private final static class CustomUserDetails  implements UserDetails {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7230555590734923391L;
		
		private final String username; 
		
		public CustomUserDetails(String username){
			this.username = username;
		}
		
		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			Collection<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
			
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + "USER");
			authorities.add(authority);
			
			return authorities;
		}

		@Override
		public String getPassword() {
			return "password";
		}

		@Override
		public String getUsername() {
			return username;
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
		
	}
}